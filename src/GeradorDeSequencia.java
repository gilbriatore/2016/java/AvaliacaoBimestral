import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GeradorDeSequencia {
	
	public static void main(String[] args) throws IOException { 
	
		/**
		 * Desenvolver um programa que grave duas tabelas de dados em um
		 * arquivo de texto. A primeira tabela ter� os n�meros �mpares 
		 * de 1 a 100 e a segunda com os n�meros pares de 1 a 100. Ambas 
		 * as tabelas devem ter 10 colunas e cada coluna ser separada 
		 * por tabula��o (\t).
		 */
		
		File arquivo = new File("tabelas.txt");
		FileWriter fw = new FileWriter(arquivo);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (int i = 1; i <= 100; i++) {
			if (i % 2 != 0){
				//n�mero �mpar
				bw.write(i + "\t");
			}
			if (i % 20 == 0){
				//quebra a linha
				bw.newLine();
			}
		}
		
		//Separa as tabelas;
		bw.newLine();
		bw.newLine();
		
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0){
				//n�mero par
				bw.write(i + "\t");
			} 
			if (i % 20 == 0){
				//quebra a linha
				bw.newLine();
			}
		}
		bw.close();
		fw.close();
		System.out.println("Arquivo gravado!");
	}
}