import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GeradorDeCartelas {
	
	public static void main(String[] args) throws IOException {

		/**
		 * Desenvolva um programa que grave duas cartelas
		 * de n�meros aleat�rios em um arquivo de texto.
		 * Cada cartela dever� ter 25 n�meros de 1 a 100
		 * distribu�dos em 5 colunas. Os n�meros n�o podem 
		 * repetir na mesma cartela e devem ser tabulados (\t).
		 */
		
		File arquivo = new File("cartelas.txt");
		FileWriter fw = new FileWriter(arquivo);
		BufferedWriter bw = new BufferedWriter(fw);
		
		Random r = new Random();
		
		String cartela1 = new String();
		String cartela2 = new String();
		
		int contador = 0;
		while(contador < 25){
			int numero = r.nextInt(100) + 1;
			String str = String.valueOf(numero);
			if (!cartela1.contains(str)){
				cartela1 += str + "\t";
				contador++;
				if (contador % 5 == 0){
					cartela1 += "\n";
				}
			}
		}
		
		bw.write(cartela1);
		bw.newLine();
		
		contador = 0;
		while(contador < 25){
			int numero = r.nextInt(100) + 1;
			String str = String.valueOf(numero);
			if (!cartela2.contains(str)){
				cartela2 += str + "\t";
				contador++;
				if (contador % 5 == 0){
					cartela2 += "\n";
				}
			}
		}
		
		bw.write(cartela2);
		bw.close();
		fw.close();
		
		System.out.println("Cartelas gravadas!");

	}
}